"use strict";

const express = require("express"),
  config = require("./config/config"),
  app = express();

app
  .use(express.urlencoded({ extended: false }))
  .use(express.json())
  .use(require("./routes/index"))
  .listen(config.port, () => {
    console.log("Servicio <test> escuchando en el puerto:", config.port);
  });