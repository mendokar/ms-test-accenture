process.env.NODE_ENV = process.env.NODE_ENV || "dev";

module.exports = {
  logs: process.env.LOGS || true,

  port: process.env.PORT || 3600,

  endpoint_rest: process.env.ENDPOINT_REST || 'http://www.red.cl/restservice/rest/getrecorrido/',

  endpoint_rest_post: process.env.ENDPOINT_REST || 'https://jsonplaceholder.typicode.com/posts'
};
