"use strict";

const
  config = require("../config/config"),
  msg = require("../lib/message"),
  request = require('request');

/**
 * 
 * @param {*} id 
 * Función que permite construir y hacer petición get
 */
async function orchTest(id) {

  const result = new msg.MessageBuilder()
    .setOrigen("test/invoke")
    .build();
  try {

    let response = await axios({
      headers: { "content-type": "application/json" },
      method: "get",
      url: `${config.endpoint_rest}${id}`,
      preambleCRLF: true,
      postambleCRLF: true
    })

    if (response !== undefined) {
      result.success = true;
      result.message = "Ejecución Exitosa";
      result.documents = {
        data: JSON.parse(response.body),
        descripcion: "aqui la descripcion..."
      };
    } else {
      result.success = false;
      result.message = "Ejecución Fallida";
    }
    return result;
  } catch (e) {
    if (config.logs) console.log("Error:", e);
    result.success = false;
    result.message = e.message || "Request failed with status code 404";
    return result;
  }

}

/**
 * 
 * @param {*} options 
 * Funcion que permite consultar el end point get que retorna datos 
 */
function axios(options) {
  return new Promise((resolve, reject) => {
    request(options, (error, response, body) => {
      if (error) return reject(error)
      return resolve({ body, response });
    });
  });
}

/**
 * 
 * @param {*} params 
 * Función que permite construir y hacer petición post
 */
async function sendPost(params) {
  const result = new msg.MessageBuilder()
    .setOrigen("test/save")
    .build();
  try {

    let response = await post({
      headers: { "content-type": "application/json" },
      method: "POST",
      url: `${config.endpoint_rest_post}`,
      preambleCRLF: true,
      postambleCRLF: true,
      data:params
    })

    if (response !== undefined) {
      result.success = true;
      result.message = "Ejecución Exitosa";
      result.documents = {
        data: JSON.parse(response.body),
        descripcion: "Petición post ejecutada correctamente."
      };
    } else {
      result.success = false;
      result.message = "Ejecución Fallida";
    }
    return result;
  } catch (e) {
    if (config.logs) console.log("Error:", e);
    result.success = false;
    result.message = e.message || "Request failed with status code 404";
    return result;
  }

}



/**
 * 
 * @param {*} options 
 * Función que permite realizar una petición post a una url placeholder ubicada en la configuracion
 */
function post (options) {  
  return new Promise((resolve, reject) => {
    request(options, (error, response, body) => {
      if (error) return reject(error)
      return resolve({ body, response });
    })
  })
}


  

module.exports = { orchTest, sendPost };
