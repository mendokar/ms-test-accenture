"use strict";

const express = require("express"),
  info = require("../../package.json"),
  routes = require("./rest"),
  msg = require("../lib/message"),
  config = require("../config/config"),
  app = express();

const service = "test";
const result = new msg.MessageBuilder().setOrigen(service).build();

app.get(`/${service}/`, (req, res) => {
  res.json("Bienvenido a mi Test");
});

app.get(`/${service}/version`, (req, res) => {
  res.json({
    service: info.name,
    description: info.description,
    version: info.version
  });
});

app.get(`/${service}/invoke/:id`, (req, res) => {
  try {
    const id = req.params.id;

    routes.orchTest(id)
      .then(respuesta => {
        res.json(respuesta);
      })
      .catch(e => res.json(e));
  } catch (e) {
    if (config.logs) console.log(">>>", e);
    result.success = false;
    result.message = e.message;
    return res.json(result);
  }
});


app.post(`/${service}/save`, function (req, res) {
  const { nombres, apellidos } = req.body;

  if (nombres && apellidos) {
    routes.sendPost(req.body).then(respuesta => {
      return res.json(respuesta);
    }).catch(err => {
      result.success = false;
      result.message = err.message;
      return res.json(result);
    });
  } else {
    result.success = false;
    result.message = "Error al guardar la información, campos necesarios.";
    return res.json(result);
  }
})

module.exports = app;
